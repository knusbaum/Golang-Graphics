package main

import (
	"image"
	"fmt"
_	"9fans.net/go/draw"
	"github.com/knusbaum/draw/draw"
	"time"
)

const (
	Left  = 0
	Right = 1
)

func setup_drawing(clickchan chan int) (*draw.Display, error) {
	errchan := make(chan error)
	display, err := draw.Init(errchan, "", "Godraw test", "800x800")
	if err != nil {
		return nil, err
	}

	mouse := display.InitMouse()
	go func() {
		for {
			ev := <-mouse.Resize
			fmt.Println("Got resize: ", ev)
			display.Attach(0)
		}
	}()

	go func() {
		for {
			ev := <-mouse.C
			if ev.Buttons & (1<<0) != 0 {
			//	fmt.Println("Button 1")
				clickchan <- Left
			} else if ev.Buttons & (1<<1) != 0 {
			//	fmt.Println("Button 2")
			} else if ev.Buttons & (1<<2) != 0 {
			//	fmt.Println("Button 3")
				clickchan <- Right
			}
		}
	}()

	return display, nil
}

func main() {

	p1 := Point3{ -50,  -50, 50}
	p2 := Point3{ -50, 50, 50}
	p3 := Point3{50, 50, 50}
	p4 := Point3{50,  -50, 50}

	p5 := Point3{ -50,  -50,   -50}
	p6 := Point3{ -50, 50,   -50}
	p7 := Point3{50, 50,   -50}
	p8 := Point3{50,  -50,   -50}

	cube := make([]Polygon, 6)
	//front
	cube[0] = []Point3{p1, p2, p3, p4}

	//left
	cube[1] = []Point3{p1, p4, p8, p5}

	//right
	cube[2] = []Point3{p2, p6, p7, p3}

	//top
	cube[3] = []Point3{p1, p5, p6, p2}

	//bottom
	cube[4] = []Point3{p4, p8, p7, p3}

	//back
	cube[5] = []Point3{p5, p6, p7, p8}

	centerLineY := Line3{
		Point3{0, -100, 0},
		Point3{0, 100, 0}}
	centerLineX := Line3{
		Point3{-100, 0, 0},
		Point3{100, 0, 0}}
	centerLine := &centerLineY

	cameraPosition := Point3{}
	cameraPosition.Z -= 300
	RotateX(cube, 20)

	clickchan := make(chan int)

	display, err := setup_drawing(clickchan)
	if err != nil {
		fmt.Println("Failed to get display: ", err)
		return
	}
	defer display.Close()
	screen := display.Image

	pix, err := draw.ParsePix("r8g8b8a8")
	if err != nil {
		fmt.Println("Failed to create pix: ", err)
		return
	}

	red, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Red)
	if err != nil {
		fmt.Println("Failed to create RED image: ", err)
		return
	}
	green, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Green)
	if err != nil {
		fmt.Println("Failed to create GREEN image: ", err)
		return
	}
	blue, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Blue)
	if err != nil {
		fmt.Println("Failed to create BLUE image: ", err)
		return
	}
	yellow, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Yellow)
	if err != nil {
		fmt.Println("Failed to create YELLOW image: ", err)
		return
	}
	purple, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Purpleblue)
	if err != nil {
		fmt.Println("Failed to create PURPLE image: ", err)
		return
	}
	black, err := display.AllocImage(image.Rect(0,0,1,1), pix, true, draw.Black)
	if err != nil {
		fmt.Println("Failed to create BLACK image: ", err)
		return
	}

	colors := []*draw.Image{red, green, blue, yellow, purple, black}

	cameraMotion := 5.0

	rotateDir := 0
	rotateVal := 0.125
	for {
		select {
		case click := <-clickchan:
			if click == Left {
				rotateDir = (rotateDir + 1) % 2
				if centerLine == &centerLineY {
					centerLine = &centerLineX
				} else {
					centerLine = &centerLineY
				}
			} else if click == Right {
				rotateVal = -rotateVal
			}
		default:
		}

		im, err := display.AllocImage(image.Rect(0,0,800,800), pix, false, draw.White)
		if err != nil {
			fmt.Println("Failed to allocate image: ", err)
		}

		centerLine.Draw(red, im, &cameraPosition)
		Draw(cube, colors, im, &cameraPosition, screen, display)
		screen.Draw(im.Bounds(), im, nil, image.Point{0,0})
		display.Flush()
		im.Free()
		time.Sleep(10 * time.Millisecond)

		if rotateDir == 0 {
			RotateY(cube, rotateVal)
		} else {
			RotateX(cube, rotateVal)
		}
		cameraPosition.Z -= cameraMotion

		if cameraMotion > 0 {
			cameraMotion -= 0.02
		}
	}
}
