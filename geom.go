package main

import (
	"math"
	"image"
_	"9fans.net/go/draw"
	"github.com/knusbaum/draw/draw"
_	"fmt"
	"sort"
_	"time"
)

type Point3 struct {
	X, Y, Z float64
}

type Point2 struct {
	X, Y float64
}

type Line3 struct {
	V1, V2 Point3
}

type Line2 struct {
	V1, V2 Point2
}

type Polygon []Point3

type Matrix3x3 struct {
	Top, Middle, Bottom Point3
}

func (l *Line2) Draw(src *draw.Image, i *draw.Image) {
	i.Line(image.Point{int(l.V1.X), int(l.V1.Y)}, image.Point{int(l.V2.X), int(l.V2.Y)},
		1,1,1,
		src, image.Point{0,0})
}

func (l *Line3) Draw(src *draw.Image, i *draw.Image, cameraPos *Point3) {
	l2 := l.ProjectFrom(cameraPos)
	l2.Draw(src, i)
}

func (p Polygon) Draw(src *draw.Image, im *draw.Image, cameraPos *Point3) {

//	for i := range p {
//		if i+1 == len(p) {
//			(&Line3{p[i], p[0]}).Draw(src, im, cameraPos)
//		} else {
//			(&Line3{p[i], p[i+1]}).Draw(src, im, cameraPos)
//		}
//	}


	pts := make([]image.Point, 0)
	for _, pt := range p {
		pt2 := pt.ProjectFrom(cameraPos)
		pts = append(pts, image.Point{int(pt2.X), int(pt2.Y)})
	}
	im.FillPoly(pts, 1, 1, 1, src, image.Point{0,0})
}

type polycolor struct {
	Polygon
	color *draw.Image
}

type BackFirst []polycolor

func (b BackFirst) Less(i, j int) bool {
	pi := b[i].Polygon
	pj := b[j].Polygon

	maxi := pi[0].Z
	maxj := pj[0].Z

	for k := range pi {
		maxi = math.Max(maxi, pi[k].Z)
	}
	for k := range pj {
		maxj = math.Max(maxj, pj[k].Z)
	}

	return maxi > maxj
}

func (b BackFirst) Swap(i, j int) {
	temp := b[i]
	b[i] = b[j]
	b[j] = temp
}

func (b BackFirst) Len() int {
	return len(b)
}

func Draw(polys []Polygon, srcs []*draw.Image, im *draw.Image, cameraPos *Point3, screen *draw.Image, display *draw.Display) {
	polysort := make([]polycolor, len(polys))
	for i, poly := range polys {
		polysort[i] = polycolor{poly, srcs[i % len(srcs)]}
	}

	sort.Sort(BackFirst(polysort))

	for _, poly := range polysort {
		poly.Polygon.Draw(poly.color, im, cameraPos)
//		screen.Draw(im.Bounds(), im, nil, image.Point{0,0})
//		display.Flush()
//		time.Sleep(500 * time.Millisecond)
	}
}

func (p *Point3) Project() Point2 {
	return Point2{ p.X, p.Y }
}

func (l *Line3) Project() Line2 {
	return Line2{ l.V1.Project(), l.V2.Project() }
}

func (p *Point3) ProjectFrom(cameraPos *Point3) Point2{
	d := Point3{
		p.X - cameraPos.X,
		p.Y - cameraPos.Y,
		p.Z - cameraPos.Z}

	f := Point3{ -400, -400, 1000 }
	return Point2{
		(f.Z/d.Z) * d.X - f.X,
		(f.Z/d.Z) * d.Y - f.Y}
}

func (l *Line3) ProjectFrom(cameraPos *Point3) Line2{
	return Line2{
		l.V1.ProjectFrom(cameraPos),
		l.V2.ProjectFrom(cameraPos)}
}

func (l *Point3) ShiftX(amount float64) Point3 {
	return Point3{l.X + amount, l.Y, l.Z}
}

func (l *Point3) ShiftY(amount float64) Point3 {
	return Point3{l.X, l.Y + amount, l.Z}
}

func (l *Point3) ShiftZ(amount float64) Point3 {
	return Point3{l.X, l.Y, l.Z + amount}
}

func (l *Line3) ShiftX(amount float64) Line3 {
	return Line3{l.V1.ShiftX(amount), l.V2.ShiftX(amount)}
}

func (l *Line3) ShiftY(amount float64) Line3 {
	return Line3{l.V1.ShiftY(amount), l.V2.ShiftY(amount)}
}
func (l *Line3) ShiftZ(amount float64) Line3 {
	return Line3{l.V1.ShiftZ(amount), l.V2.ShiftZ(amount)}
}

func (l *Line3) RotateX(theta float64) Line3 {
	return Line3{ l.V1.RotateX(theta), l.V2.RotateX(theta) }
}

func (l *Line3) RotateY(theta float64) Line3 {
	return Line3{ l.V1.RotateY(theta), l.V2.RotateY(theta) }
}

func (l *Line3) RotateZ(theta float64) Line3 {
	return Line3{ l.V1.RotateZ(theta), l.V2.RotateZ(theta) }
}

func RotateX(mesh []Polygon, theta float64) {
	topmost := mesh[0][0].Y
	bottommost := mesh[0][0].Y
	nearmost := mesh[0][0].Z
	farmost := mesh[0][0].Z

	for _, poly := range mesh {
		for _, pt := range poly {
			topmost = math.Min(topmost, pt.Y)
			bottommost = math.Max(bottommost, pt.Y)
			nearmost = math.Min(nearmost, pt.Z)
			farmost = math.Max(farmost, pt.Z)
		}
	}

	shiftY := -((topmost + bottommost) / 2)
	shiftZ := -((nearmost + farmost) / 2)

	for i := range mesh {
		for j := range mesh[i] {
			shifted := (&mesh[i][j]).ShiftY(shiftY)
			shifted = (&shifted).ShiftZ(shiftZ)
			rotated := (&shifted).RotateX(theta)
			shiftback := (&rotated).ShiftY(-shiftY)
			shiftback = (&shiftback).ShiftZ(-shiftZ)
			mesh[i][j] = shiftback
		}
	}
}

func RotateY(mesh []Polygon, theta float64) {
	leftmost := mesh[0][0].X
	rightmost := mesh[0][0].X
	nearmost := mesh[0][0].Z
	farmost := mesh[0][0].Z

	for _, poly := range mesh {
		for _, pt := range poly {
			leftmost = math.Min(leftmost, pt.X)
			rightmost = math.Max(rightmost, pt.X)
			nearmost = math.Min(nearmost, pt.Z)
			farmost = math.Max(farmost, pt.Z)
		}
	}

	shiftX := -((leftmost + rightmost) / 2)
	shiftZ := -((nearmost + farmost) / 2)

	for i := range mesh {
		for j := range mesh[i] {
			shifted := (&mesh[i][j]).ShiftX(shiftX)
			shifted = (&shifted).ShiftZ(shiftZ)
			rotated := (&shifted).RotateY(theta)
			shiftback := (&rotated).ShiftX(-shiftX)
			shiftback = (&shiftback).ShiftZ(-shiftZ)
			mesh[i][j] = shiftback
		}
	}
}

func RotateZ(mesh []Polygon, theta float64) {
	leftmost := mesh[0][0].X
	rightmost := mesh[0][0].X
	topmost := mesh[0][0].Y
	bottommost := mesh[0][0].Y

	for _, poly := range mesh {
		for _, pt := range poly {
			leftmost = math.Min(leftmost, pt.X)
			rightmost = math.Max(rightmost, pt.X)
			topmost = math.Min(topmost, pt.Y)
			bottommost = math.Max(bottommost, pt.Y)
		}
	}

	shiftX := -((leftmost + rightmost) / 2)
	shiftY := -((topmost + bottommost) / 2)

	for i := range mesh {
		for j := range mesh[i] {
			shifted := (&mesh[i][j]).ShiftX(shiftX)
			shifted = (&shifted).ShiftY(shiftY)
			rotated := (&shifted).RotateZ(theta)
			shiftback := (&rotated).ShiftX(-shiftX)
			shiftback = (&shiftback).ShiftY(-shiftY)
			mesh[i][j] = shiftback
		}
	}
}

//  Z axis
//  |cos θ   -sin θ   0| |x|   |x cos θ - y sin θ|   |x'|
//  |sin θ    cos θ   0| |y| = |x sin θ + y cos θ| = |y'|
//  |  0       0      1| |z|   |        z        |   |z'|
//
//  Y axis
//  | cos θ    0   sin θ| |x|   | x cos θ + z sin θ|   |x'|
//  |   0      1       0| |y| = |         y        | = |y'|
//  |-sin θ    0   cos θ| |z|   |-x sin θ + z cos θ|   |z'|
//
//  X axis
//  |1     0           0| |x|   |        x        |   |x'|
//  |0   cos θ    -sin θ| |y| = |y cos θ - z sin θ| = |y'|
//  |0   sin θ     cos θ| |z|   |y sin θ + z cos θ|   |z'|

func (p *Point3) RotateZ(theta float64) Point3 {
	matrix := Matrix3x3{
		Point3{ math.Cos(theta), -math.Sin(theta), 0},
		Point3{ math.Sin(theta),  math.Cos(theta), 0},
		Point3{               0,                0, 1}}
	return matrix.Multiply(p)
}

func (p *Point3) RotateY(theta float64) Point3 {
	matrix := Matrix3x3{
		Point3{  math.Cos(theta), 0, math.Sin(theta)},
		Point3{                0, 1,               0},
		Point3{ -math.Sin(theta), 0, math.Cos(theta)}}
	return matrix.Multiply(p)
}

func (p *Point3) RotateX(theta float64) Point3 {
	matrix := Matrix3x3{
		Point3{ 1,                0,                0},
		Point3{ 0,  math.Cos(theta), -math.Sin(theta)},
		Point3{ 0,  math.Sin(theta),  math.Cos(theta)}}
	return matrix.Multiply(p)
}

func (m *Matrix3x3) Multiply(v *Point3) Point3{
	var top_res = (m.Top.X * v.X) + (m.Top.Y * v.Y) + (m.Top.Z * v.Z)
	var mid_res = (m.Middle.X * v.X) + (m.Middle.Y * v.Y) + (m.Middle.Z * v.Z)
	var bottom_res = (m.Bottom.X * v.X) + (m.Bottom.Y * v.Y) + (m.Bottom.Z * v.Z)
	return Point3{top_res, mid_res, bottom_res}
}

// Old Draw Code
//func (l *Line2) Draw(i *image.NRGBA) {
//	a := l.V1
//	b := l.V2
//
//	m := (a.Y - b.Y) / (a.X - b.X)
//	f := func(x float64)float64 {
//		return m * (x - b.X) + b.Y
//	}
//
//	var start float64
//	var end float64
//
//	if(a.X < b.X) {
//		start = a.X
//		end = b.X
//	} else {
//		start = b.X
//		end = a.X
//	}
//
//	for x := start; x <= end; x++ {
//		y := f(x)
//		i.Set(int(x), int(y), color.NRGBA{255,0,0,255})
//	}
//}
